VERSION = '0.1'
APPNAME = 'mrf-link'

def options(ctx):
    ctx.add_option('--with-fw', action='store_true', default=False, help='build firmware')
    ctx.recurse('fw')

def configure(ctx):
    if ctx.options.with_fw:
        ctx.recurse('fw')
        ctx.env.WITH_FW = 1

def build(ctx):
    if ctx.env.WITH_FW:
        ctx.recurse('fw')
