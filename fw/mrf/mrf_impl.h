/*
 * Copyright 2010 Grygoriy Fuchedzhy <grygoriy.fuchedzhy@gmail.com>
 *
 * This file is part of mrf-link.
 *
 * mrf-link is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * mrf-link is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mrf-link. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MRF_IMPL__
#define __MRF_IMPL__

#include <mrf/mrf.h>

#include <util/delay.h>
#include <avr/spi.h>
#include <avr/interrupt.h>

uint16_t mrf_short_addr = MRF_SHORT_ADDR;
uint16_t mrf_pan_id = MRF_PAN_ID;
uint8_t mrf_seq_num = 0;
uint8_t mrf_rssi = 0, mrf_lqi = 0;
volatile uint8_t mrf_status = 0;

// set given short register to given value via spi
void mrf_write_short_addr(uint8_t addr, uint8_t val)
{
   uint8_t sreg = SREG;
   cli();
   spi_select();
   spi_shift_byte(0x1 | (addr << 1));
   spi_shift_byte(val);
   spi_deselect();
   SREG = sreg;
}

// get and return value of given short register via spi
uint8_t mrf_read_short_addr(uint8_t addr)
{
   uint8_t sreg = SREG;
   cli();
   spi_select();
   spi_shift_byte(addr << 1);
   uint8_t res = spi_shift_byte(0x00);
   spi_deselect();
   SREG = sreg;
   return res;
}

// set given long register to given value via spi
void mrf_write_long_addr(uint16_t addr, uint8_t val)
{
   uint8_t sreg = SREG;
   cli();
   spi_select();
   spi_shift_byte(0x80 | (addr >> 3));
   spi_shift_byte(0x10 | (addr << 5));
   spi_shift_byte(val);
   spi_deselect();
   SREG = sreg;
}

// get and return value of given long register via spi
uint8_t mrf_read_long_addr(uint16_t addr)
{
   uint8_t sreg = SREG;
   cli();
   spi_select();
   spi_shift_byte(0x80 | (addr >> 3));
   spi_shift_byte(addr << 5);
   uint8_t res = spi_shift_byte(0x00);
   spi_deselect();
   SREG = sreg;
   return res;
}

// set channel, ch must be: 0x0 - 0xF
void mrf_set_channel(uint8_t ch)
{
   // 0x2 - recommended value
   mrf_set_reg(MRF_R_RFCON0, (ch<<4) | 0x2);
   // reset rf module
   mrf_set_reg(MRF_R_RFCTL, _BV(MRF_B_RFRST));
   mrf_set_reg(MRF_R_RFCTL, 0);
   _delay_us(192);
}

// reset and initialize radio
void mrf_init()
{
   // software reset, the bits will be automatically cleared by hardware.
   mrf_set_reg(MRF_R_SOFTRST, _BV(MRF_B_RSTPWR) | _BV(MRF_B_RSTBB) | _BV(MRF_B_RSTMAC));

   // register initialization
   mrf_set_reg(MRF_R_PACON2, _BV(MRF_B_FIFOEN) | MRF_RV_TXONTS);
   mrf_set_reg(MRF_R_TXSTBL, MRF_RV_RFSTBL | MRF_RV_MSIFS);
   mrf_set_reg(MRF_R_RFCON1, MRF_RV_VCOOPT);
   mrf_set_reg(MRF_R_RFCON2, _BV(MRF_B_PLLEN));
   mrf_set_reg(MRF_R_RFCON6, _BV(MRF_B_TXFIL) | _BV(MRF_B_20MRECVR));
   mrf_set_reg(MRF_R_RFCON7, MRF_V_SLPCLKSEL_100kHz);
   mrf_set_reg(MRF_R_RFCON8, _BV(MRF_B_RFVCO));
   mrf_set_reg(MRF_R_SLPCON1, _BV(MRF_B_CLKOUTEN) | 0x1);
   mrf_set_reg(MRF_R_BBREG2, MRF_V_CCAMODE_ENERGY_ABOVE_THRESHOLD);
   mrf_set_reg(MRF_R_CCAEDTH, MRF_RV_CCAEDTH);
   mrf_set_reg(MRF_R_BBREG6, _BV(MRF_B_RSSIMODE2));

   // read and set address
   mrf_set_reg(MRF_R_PANIDL, mrf_pan_id);
   mrf_set_reg(MRF_R_PANIDH, mrf_pan_id>>8);
   mrf_set_reg(MRF_R_SADRL, mrf_short_addr);
   mrf_set_reg(MRF_R_SADRH, mrf_short_addr>>8);

   mrf_set_channel(0);
}

void mrf_tx(uint16_t pan_id, uint16_t short_addr, const uint8_t* data, uint8_t data_length)
{
   uint16_t addr = MRF_BUF_TX_NORMAL;

   // frame control + seq.num. + addressing fields
   const uint8_t hdr_length = 2 + 1 + 2*(IEEE802_15_4_SHORT_ADDR_LENGTH + IEEE802_15_4_PAN_ID_LENGTH);

   mrf_write_long_addr(addr++, hdr_length);
   mrf_write_long_addr(addr++, hdr_length + data_length);

   // frame control
   mrf_write_long_addr(addr++, IEEE802_15_4_DATA | IEEE802_15_4_ACK_REQ);
   mrf_write_long_addr(addr++, IEEE802_15_4_SHORT_DESTADDR | IEEE802_15_4_SHORT_SRCADDR | IEEE802_15_4_2003_VERSION);

   // sequence number
   mrf_write_long_addr(addr++, mrf_seq_num++);

   // dest pan id
   mrf_write_long_addr(addr++, pan_id);
   mrf_write_long_addr(addr++, pan_id>>8);

   // dest short addr
   mrf_write_long_addr(addr++, short_addr);
   mrf_write_long_addr(addr++, short_addr>>8);

   // source pan id
   mrf_write_long_addr(addr++, mrf_pan_id);
   mrf_write_long_addr(addr++, mrf_pan_id>>8);

   // source short addr
   mrf_write_long_addr(addr++, mrf_short_addr);
   mrf_write_long_addr(addr++, mrf_short_addr>>8);

   for(uint8_t i = 0; i < data_length; ++i)
      mrf_write_long_addr(addr++, data[i]);

   mrf_status &= ~_BV(MRF_B_TXN);
   // transmit frame, request acknowledgement
   mrf_set_reg(MRF_R_TXNCON, _BV(MRF_B_ACKREQ) | _BV(MRF_B_TRIG));
}

uint8_t mrf_rx(uint16_t *pan_id, uint16_t *short_addr, uint8_t* data, uint8_t data_length)
{
   // relying on fact that header is in same format as in tx function
   // frame control + seq.num. + addressing fields
   const uint8_t hdr_length = 2 + 1 + 2*(IEEE802_15_4_SHORT_ADDR_LENGTH + IEEE802_15_4_PAN_ID_LENGTH);
   const uint16_t data_offset = MRF_BUF_RX + 1 + hdr_length;

   // disable receiving packets off air
   mrf_set_reg(MRF_R_BBREG1, _BV(MRF_B_RXDECINV));

   // -2 because of fcs bytes
   uint8_t length = mrf_read_long_addr(MRF_BUF_RX) - hdr_length - 2;

   if(0 != pan_id)
      *pan_id = mrf_read_long_addr(MRF_BUF_RX + 8) + (mrf_read_long_addr(MRF_BUF_RX + 9)<<8);
   if(0 != short_addr)
      *short_addr = mrf_read_long_addr(MRF_BUF_RX + 10) + (mrf_read_long_addr(MRF_BUF_RX + 11)<<8);

   uint8_t i = 0;
   for(; length != 0 && i < data_length; i++, length--)
      data[i] = mrf_read_long_addr(data_offset + i);

   mrf_lqi = mrf_read_long_addr(data_offset + i + 2);
   mrf_rssi = mrf_read_long_addr(data_offset + i + 3);

   // enable receiving packets off air
   mrf_set_reg(MRF_R_BBREG1, 0);

   mrf_status &= ~_BV(MRF_B_RX);
   return i;
}

#endif // __MRF_IMPL__
