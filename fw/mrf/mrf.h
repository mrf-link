/*
 * Copyright 2010 Grygoriy Fuchedzhy <grygoriy.fuchedzhy@gmail.com>
 *
 * This file is part of mrf-link.
 *
 * mrf-link is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * mrf-link is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mrf-link. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MRF__
#define __MRF__

#include <mrf/defs.h>
#include <inttypes.h>

uint8_t mrf_read_short_addr(uint8_t addr);
uint8_t mrf_read_long_addr(uint16_t addr);
void mrf_write_short_addr(uint8_t addr, uint8_t val);
void mrf_write_long_addr(uint16_t addr, uint8_t val);
void mrf_set_channel(uint8_t ch);
void mrf_init();
void mrf_tx(uint16_t pan_id, uint16_t short_addr, const uint8_t* data, uint8_t data_length);
uint8_t mrf_rx(uint16_t *pan_id, uint16_t *short_addr, uint8_t* data, uint8_t data_length);

extern uint8_t mrf_rssi, mrf_lqi;
extern volatile uint8_t mrf_status;

// wrapper function, with constant addr parameter and optimization compiles into
// direct call to appropriate function
__attribute__ ((always_inline)) static void
mrf_set_reg(uint16_t addr, uint8_t val)
{
   if(addr < 0x100)
      mrf_write_short_addr(addr, val);
   else
      mrf_write_long_addr(addr, val);
}

// wrapper function, with constant addr parameter and optimization compiles into
// direct call to appropriate function
__attribute__ ((always_inline)) static uint8_t
mrf_get_reg(uint16_t addr)
{
   if(addr < 0x100)
      return mrf_read_short_addr(addr);
   else
      return mrf_read_long_addr(addr);
}

// tx power, small scale must be: 0x0 - 0x1F, small_scale = 0x0 is max power
// large scale must be: 0x0 - 0x3, large_scale = 0x0 is max power
static inline void mrf_set_tx_power(uint8_t large_scale, uint8_t small_scale)
{
   mrf_set_reg(MRF_R_RFCON3, (large_scale<<6) | (small_scale<<3));
}

// get txretry number
static inline uint8_t mrf_get_txretry()
{
   return mrf_get_reg(MRF_R_TXSTAT) >> 6;
}

static inline uint8_t mrf_is_tx_successful()
{
   return !(mrf_get_reg(MRF_R_TXSTAT) & _BV(MRF_B_TXN));
}

static inline uint8_t mrf_is_rx_complete()
{
   if(bit_is_set(mrf_status, MRF_B_RX))
      return 1;
   mrf_status |= mrf_get_reg(MRF_R_INTSTAT);
   return bit_is_set(mrf_status, MRF_B_RX);
}

static inline void mrf_wait_rx_complete()
{
   while(!mrf_is_rx_complete());
}

static inline uint8_t mrf_is_tx_complete()
{
   if(bit_is_set(mrf_status, MRF_B_TXN))
      return 1;
   mrf_status |= mrf_get_reg(MRF_R_INTSTAT);
   return bit_is_set(mrf_status, MRF_B_TXN);
}

static inline void mrf_wait_tx_complete()
{
   while(!mrf_is_tx_complete());
}

#endif // __MRF__
