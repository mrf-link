/*
 * Copyright 2010 Grygoriy Fuchedzhy <grygoriy.fuchedzhy@gmail.com>
 *
 * This file is part of mrf-link.
 *
 * mrf-link is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * mrf-link is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mrf-link. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MRF_DEFS__
#define __MRF_DEFS__

// BUF_ means buffer address
// RV_ means recommended value
// R_ means address register
// B_ means bit
// V_ means value

// defs for frame control bytes
// 1st byte
#define IEEE802_15_4_BEACON        0x0
#define IEEE802_15_4_DATA          0x1
#define IEEE802_15_4_ACK           0x2
#define IEEE802_15_4_COMMAND       0x3
#define IEEE802_15_4_SECURITY      _BV(3)
#define IEEE802_15_4_FRAME_PENDING _BV(4)
#define IEEE802_15_4_ACK_REQ       _BV(5)
#define IEEE802_15_4_PAN_ID_COMPR  _BV(6)

// 2nd byte
#define IEEE802_15_4_DESTADDR_MASK       0xC
#define IEEE802_15_4_DESTADDR_NOT_PESENT (0x0<<2)
#define IEEE802_15_4_SHORT_DESTADDR      (0x2<<2)
#define IEEE802_15_4_EXTENDED_DESTADDR   (0x3<<2)
#define IEEE802_15_4_SRCADDR_MASK        0xC0
#define IEEE802_15_4_SRCADDR_NOT_PESENT  (0x0<<6)
#define IEEE802_15_4_SHORT_SRCADDR       (0x2<<6)
#define IEEE802_15_4_EXTENDED_SRCADDR    (0x3<<6)
#define IEEE802_15_4_2003_VERSION        (0x0<<4)
#define IEEE802_15_4_VERSION             (0x1<<4)

#define IEEE802_15_4_EXT_ADDR_LENGTH   8
#define IEEE802_15_4_SHORT_ADDR_LENGTH 2
#define IEEE802_15_4_PAN_ID_LENGTH     2

#define MRF_BUF_TX_NORMAL 0x0
#define MRF_BUF_TX_BEACON 0x80
#define MRF_BUF_TX_GTS1   0x100
#define MRF_BUF_TX_GTS2   0x180
#define MRF_BUF_SECURITY  0x280
#define MRF_BUF_RX        0x300

// short address registers
#define MRF_R_RXMCR     0x00
#define MRF_R_PANIDL    0x01
#define MRF_R_PANIDH    0x02
#define MRF_R_SADRL     0x03
#define MRF_R_SADRH     0x04
#define MRF_R_EADR0     0x05
#define MRF_R_EADR1     0x06
#define MRF_R_EADR2     0x07
#define MRF_R_EADR3     0x08
#define MRF_R_EADR4     0x09
#define MRF_R_EADR5     0x0A
#define MRF_R_EADR6     0x0B
#define MRF_R_EADR7     0x0C
#define MRF_R_RXFLUSH   0x0D
#define MRF_R_ORDER     0x10
#define MRF_R_TXMCR     0x11
#define MRF_R_ACKTMOUT  0x12
#define MRF_R_ESLOTG1   0x13
#define MRF_R_SYMTICKL  0x14
#define MRF_R_SYMTICKH  0x15
#define MRF_R_PACON0    0x16
#define MRF_R_PACON1    0x17
#define MRF_R_PACON2    0x18
#define MRF_R_TXBCON0   0x1A
#define MRF_R_TXNCON    0x1B
#define MRF_R_TXG1CON   0x1C
#define MRF_R_TXG2CON   0x1D
#define MRF_R_ESLOTG23  0x1E
#define MRF_R_ESLOTG45  0x1F
#define MRF_R_ESLOTG67  0x20
#define MRF_R_TXPEND    0x21
#define MRF_R_WAKECON   0x22
#define MRF_R_FRMOFFSET 0x23
#define MRF_R_TXSTAT    0x24
#define MRF_R_TXBCON1   0x25
#define MRF_R_GATECLK   0x26
#define MRF_R_TXTIME    0x27
#define MRF_R_HSYMTMRL  0x28
#define MRF_R_HSYMTMRH  0x29
#define MRF_R_SOFTRST   0x2A
#define MRF_R_SECCON0   0x2C
#define MRF_R_SECCON1   0x2D
#define MRF_R_TXSTBL    0x2E
#define MRF_R_RXSR      0x30
#define MRF_R_INTSTAT   0x31
#define MRF_R_INTCON    0x32
#define MRF_R_GPIO      0x33
#define MRF_R_TRISGPIO  0x34
#define MRF_R_SLPACK    0x35
#define MRF_R_RFCTL     0x36
#define MRF_R_SECCR2    0x37
#define MRF_R_BBREG0    0x38
#define MRF_R_BBREG1    0x39
#define MRF_R_BBREG2    0x3A
#define MRF_R_BBREG3    0x3B
#define MRF_R_BBREG4    0x3C
#define MRF_R_BBREG6    0x3E
#define MRF_R_CCAEDTH   0x3F

// long address registers
#define MRF_R_RFCON0    0x200
#define MRF_R_RFCON1    0x201
#define MRF_R_RFCON2    0x202
#define MRF_R_RFCON3    0x203
#define MRF_R_RFCON5    0x205
#define MRF_R_RFCON6    0x206
#define MRF_R_RFCON7    0x207
#define MRF_R_RFCON8    0x208
#define MRF_R_SLPCAL0   0x209
#define MRF_R_SLPCAL1   0x20A
#define MRF_R_SLPCAL2   0x20B
#define MRF_R_RFSTATE   0x20F
#define MRF_R_RSSI      0x210
#define MRF_R_SLPCON0   0x211
#define MRF_R_SLPCON1   0x220
#define MRF_R_WAKETIMEL 0x222
#define MRF_R_WAKETIMEH 0x223
#define MRF_R_REMCNTL   0x224
#define MRF_R_REMCNTH   0x225
#define MRF_R_MAINCNT0  0x226
#define MRF_R_MAINCNT1  0x227
#define MRF_R_MAINCNT2  0x228
#define MRF_R_MAINCNT3  0x229
#define MRF_R_TESTMODE  0x22F
#define MRF_R_ASSOEADR0 0x230
#define MRF_R_ASSOEADR1 0x231
#define MRF_R_ASSOEADR2 0x232
#define MRF_R_ASSOEADR3 0x233
#define MRF_R_ASSOEADR4 0x234
#define MRF_R_ASSOEADR5 0x235
#define MRF_R_ASSOEADR6 0x236
#define MRF_R_ASSOEADR7 0x237
#define MRF_R_ASSOSADR0 0x238
#define MRF_R_ASSOSADR1 0x239
#define MRF_R_UPNONCE0  0x240
#define MRF_R_UPNONCE1  0x241
#define MRF_R_UPNONCE2  0x242
#define MRF_R_UPNONCE3  0x243
#define MRF_R_UPNONCE4  0x244
#define MRF_R_UPNONCE5  0x245
#define MRF_R_UPNONCE6  0x246
#define MRF_R_UPNONCE7  0x247
#define MRF_R_UPNONCE8  0x248
#define MRF_R_UPNONCE9  0x249
#define MRF_R_UPNONCE10 0x24A
#define MRF_R_UPNONCE11 0x24B
#define MRF_R_UPNONCE12 0x24C

// bits and values for registers
// MRF_R_RXMCR
#define MRF_B_NOACKRSP  5
#define MRF_B_PANCOORD  3
#define MRF_B_COORD     2
#define MRF_B_ERRPKT    1
#define MRF_B_PROMI     0

// MRF_R_RXFLUSH
#define MRF_B_WAKEPOL  6
#define MRF_B_WAKEPAD  5
#define MRF_B_CMDONLY  3
#define MRF_B_DATAONLY 2
#define MRF_B_BCNONLY  1
#define MRF_B_RXFLUSH  0

// MRF_R_TXMCR
#define MRF_B_NOCSMA    7
#define MRF_B_BATLIFEXT 6
#define MRF_B_SLOTTED   5

// MRF_R_ACKTMOUT
#define MRF_B_DRPACK 7

// MRF_R_PACON2
#define MRF_B_FIFOEN  7
#define MRF_RV_TXONTS (0x6<<2)

// MRF_R_TXNCON
#define MRF_B_FPSTAT    4
#define MRF_B_INDIRECT  3

// MRF_R_TXNCON
// MRF_R_TXG1CON
// MRF_R_TXG2CON
#define MRF_B_ACKREQ 2

// MRF_R_TXNCON
// MRF_R_TXG1CON
// MRF_R_TXG2CON
// MRF_R_TXBCON0
#define MRF_B_SECEN  1
#define MRF_B_TRIG   0

// MRF_R_TXPEND
#define MRF_B_GTSSWITCH 1
#define MRF_B_FPACK     0

// MRF_R_WAKECON
#define MRF_B_IMMWAKE 7
#define MRF_B_REGWAKE 6

// MRF_R_TXSTAT
#define MRF_B_CCAFAIL  5
#define MRF_B_TXG2FNT  4
#define MRF_B_TXG1FNT  3
#define MRF_B_TXG2STAT 2
#define MRF_B_TXG1STAT 1
#define MRF_B_TXNSTAT  0

// MRF_R_TXBCON1
#define MRF_B_TXBMSK 7
#define MRF_B_WU_BCN 6

// MRF_R_GATECLK
#define MRF_B_GTSON 3

// MRF_R_SOFTRST
#define MRF_B_RSTPWR 2
#define MRF_B_RSTBB  1
#define MRF_B_RSTMAC 0

// MRF_R_SECCON0
#define MRF_B_SECIGNORE 7
#define MRF_B_SECSTART  6
#define MRF_V_TXNCIPHER_AES_CBC_MAC_32  0x7
#define MRF_V_TXNCIPHER_AES_CBC_MAC_64  0x6
#define MRF_V_TXNCIPHER_AES_CBC_MAC_128 0x5
#define MRF_V_TXNCIPHER_AES_CCM_32      0x4
#define MRF_V_TXNCIPHER_AES_CCM_64      0x3
#define MRF_V_TXNCIPHER_AES_CCM_128     0x2
#define MRF_V_TXNCIPHER_AES_CTR         0x1
#define MRF_V_TXNCIPHER_NONE            0x0
#define MRF_V_RXCIPHER_AES_CBC_MAC_32  (0x7<<3)
#define MRF_V_RXCIPHER_AES_CBC_MAC_64  (0x6<<3)
#define MRF_V_RXCIPHER_AES_CBC_MAC_128 (0x5<<3)
#define MRF_V_RXCIPHER_AES_CCM_32      (0x4<<3)
#define MRF_V_RXCIPHER_AES_CCM_64      (0x3<<3)
#define MRF_V_RXCIPHER_AES_CCM_128     (0x2<<3)
#define MRF_V_RXCIPHER_AES_CTR         (0x1<<3)
#define MRF_V_RXCIPHER_NONE            (0x0<<3)

// MRF_R_SECCON1
#define MRF_B_DISDEC 1
#define MRF_B_DISENC 0
#define MRF_V_TXBCIPHER_AES_CBC_MAC_32  (0x7<<4)
#define MRF_V_TXBCIPHER_AES_CBC_MAC_64  (0x6<<4)
#define MRF_V_TXBCIPHER_AES_CBC_MAC_128 (0x5<<4)
#define MRF_V_TXBCIPHER_AES_CCM_32      (0x4<<4)
#define MRF_V_TXBCIPHER_AES_CCM_64      (0x3<<4)
#define MRF_V_TXBCIPHER_AES_CCM_128     (0x2<<4)
#define MRF_V_TXBCIPHER_AES_CTR         (0x1<<4)
#define MRF_V_TXBCIPHER_NONE            (0x0<<4)

// MRF_R_TXSTBL
#define MRF_RV_RFSTBL (0x9<<4)
#define MRF_RV_MSIFS  0x3

// MRF_R_RXSR
#define MRF_B_UPSECERR 6
#define MRF_B_BATIND   5

// MRF_R_INTSTAT
// MRF_R_INTCON
#define MRF_B_SLP     7
#define MRF_B_WAKE    6
#define MRF_B_HSYMTMR 5
#define MRF_B_SEC     4
#define MRF_B_RX      3
#define MRF_B_TXG2    2
#define MRF_B_TXG1    1
#define MRF_B_TXN     0

// MRF_R_GPIO
// MRF_R_TRISGPIO
#define MRF_B_GP5 5
#define MRF_B_GP4 4
#define MRF_B_GP3 3
#define MRF_B_GP2 2
#define MRF_B_GP1 1
#define MRF_B_GP0 0

// MRF_R_SLPACK
#define MRF_B_SLPACK 7

// MRF_R_RFCTL
#define MRF_B_RFRST 2

// MRF_R_SECCR2
#define MRF_B_UPDEC 7
#define MRF_B_UPENC 6
#define MRF_V_TXG1CIPHER_AES_CBC_MAC_32  0x7
#define MRF_V_TXG1CIPHER_AES_CBC_MAC_64  0x6
#define MRF_V_TXG1CIPHER_AES_CBC_MAC_128 0x5
#define MRF_V_TXG1CIPHER_AES_CCM_32      0x4
#define MRF_V_TXG1CIPHER_AES_CCM_64      0x3
#define MRF_V_TXG1CIPHER_AES_CCM_128     0x2
#define MRF_V_TXG1CIPHER_AES_CTR         0x1
#define MRF_V_TXG1CIPHER_NONE            0x0
#define MRF_V_TXG2CIPHER_AES_CBC_MAC_32  (0x7<<3)
#define MRF_V_TXG2CIPHER_AES_CBC_MAC_64  (0x6<<3)
#define MRF_V_TXG2CIPHER_AES_CBC_MAC_128 (0x5<<3)
#define MRF_V_TXG2CIPHER_AES_CCM_32      (0x4<<3)
#define MRF_V_TXG2CIPHER_AES_CCM_64      (0x3<<3)
#define MRF_V_TXG2CIPHER_AES_CCM_128     (0x2<<3)
#define MRF_V_TXG2CIPHER_AES_CTR         (0x1<<3)
#define MRF_V_TXG2CIPHER_NONE            (0x0<<3)

// MRF_R_BBREG0
#define MRF_B_TURBO 0

// MRF_R_BBREG1
#define MRF_B_RXDECINV 2

// MRF_R_BBREG2
#define MRF_V_CCAMODE_CARRIER_SENSE                             0x40
#define MRF_V_CCAMODE_ENERGY_ABOVE_THRESHOLD                    0x80
#define MRF_V_CCAMODE_CARRIER_SENSE_WITH_ENERGY_ABOVE_THRESHOLD 0xC0

// MRF_R_BBREG6
#define MRF_B_RSSIMODE1 7
#define MRF_B_RSSIMODE2 6
#define MRF_B_RSSIRDY   0

// MRF_R_CCAEDTH
#define MRF_RV_CCAEDTH 0x69

// MRF_R_RFCON0
#define MRF_B_RFRST 2

// MRF_R_RFCON1
#define MRF_RV_VCOOPT 0x1

// MRF_R_RFCON2
#define MRF_B_PLLEN 7

// MRF_R_RFCON6
#define MRF_B_TXFIL    7
#define MRF_B_20MRECVR 4
#define MRF_B_BATEN    3

// MRF_R_RFCON7
#define MRF_V_SLPCLKSEL_100kHz 0x80
#define MRF_V_SLPCLKSEL_32kHz  0x40

// MRF_R_RFCON8
#define MRF_B_RFVCO    4

// MRF_R_SLPCAL2
#define MRF_B_SLPCALRDY 7
#define MRF_B_SLPCALEN  4

// MRF_R_RFSTATE
#define MRF_V_RFSTATE_RTSEL2 0xE0
#define MRF_V_RFSTATE_RTSEL1 0xC0
#define MRF_V_RFSTATE_RX     0xA0
#define MRF_V_RFSTATE_TX     0x80
#define MRF_V_RFSTATE_CALVCO 0x60
#define MRF_V_RFSTATE_SLEEP  0x40
#define MRF_V_RFSTATE_CALFIL 0x20
#define MRF_V_RFSTATE_RESET  0x00

// MRF_R_SLPCON0
#define MRF_B_INTEDGE  1
#define MRF_B_SLPCLKEN 0

// MRF_R_SLPCON1
#define MRF_B_CLKOUTEN 5

// MRF_R_MAINCNT3
#define MRF_B_STARTCNT 7

// MRF_R_TESTMODE
#define MRF_V_TESTMODE_CONTROL_EXT_PA_LNA 0xF
#define MRF_V_TESTMODE_SINGLE_TONE        0xD
#define MRF_V_TESTMODE_NORMAL             0x8

#endif // __MRF_DEFS__
